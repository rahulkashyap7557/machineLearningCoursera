function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta, 1), 1);


% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%
L = eye(length(theta));
L(1,1) = 0;
ltheta = L*theta;
X_temp = X;
J = (X_temp*theta - y)' * (X_temp*theta - y)/(2*m) + (ltheta' * ltheta)*(0.5*lambda/m);
grad = (1/m)* X_temp' * (X_temp*theta - y) + (lambda/m)*(ltheta);












% =========================================================================

grad = grad(:);

end
